package com.adcoretechnologies.buzuby.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adcoretechnologies.buzuby.R;
import com.adcoretechnologies.buzuby.util.Const;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterNotification extends
        RecyclerView.Adapter<AdapterNotification.ViewHolder> {
    Context context;

    private List<BoNotification> allItems;

    public AdapterNotification(List<BoNotification> allItems) {
        this.allItems = allItems;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.item_notification,
                parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final BoNotification post = getItem(position);
        holder.tvTitle.setText(post.getTitle());
        holder.tvMessage.setText(post.getMessage());
        holder.tvPostedOn.setText(post.getTimestamp());

    }

    @Override
    public int getItemCount() {
        return allItems.size();
    }

    public BoNotification getItem(int position) {
        return allItems.get(position);
    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvPostedOn)
        TextView tvPostedOn;
        @BindView(R.id.tvMessage)
        TextView tvMessage;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }
}
