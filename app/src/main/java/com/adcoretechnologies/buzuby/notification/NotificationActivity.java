package com.adcoretechnologies.buzuby.notification;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.BaseActivity;
import com.adcoretechnologies.buzuby.R;
import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NotificationActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvPostStatus)
    TextView tvPostStatus;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.llStatus)
    LinearLayout llStatus;
    private ArrayList<BoNotification> allItems;
    private AdapterNotification adapter;
    IAppService methods;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initViews();
    }

    public void initViews() {
        allItems = new ArrayList<>();
        adapter = new AdapterNotification(allItems);
        dialog = new ProgressDialog(this);
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        fillData();

    }

    private void fillData() {
        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Getting Notifications");
        dialog.show();

        methods.getAllPushNotification(Const.ACTION_GET_ALL_NOTIFICATION,  Common.getUserId(getApplicationContext()), new Callback<PojoPush>() {
            @Override
            public void success(PojoPush pojoEvent, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojoEvent.getStatus();
                String message = pojoEvent.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    BindViewEvents(pojoEvent.getAllEvents());
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while getting notifications", error);

                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }
    private void BindViewEvents(ArrayList<BoNotification> allEvents) {
        updateAdapter(allEvents);




    }

    private void showProgress() {
        tvPostStatus.setText("Fetching posts from server");
        llStatus.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void updateAdapter(List<BoNotification> allItems) {
        if (allItems != null && allItems.size() > 0) {
            adapter.notifyDataSetChanged();
            recyclerView.setVisibility(View.VISIBLE);
            llStatus.setVisibility(View.GONE);
        } else {
            tvPostStatus.setText("No notification to show");
            llStatus.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        }
    }


}
