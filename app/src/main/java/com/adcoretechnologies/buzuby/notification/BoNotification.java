package com.adcoretechnologies.buzuby.notification;

import org.parceler.Parcel;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Irfan on 18/12/16.
 */

@Parcel
public class BoNotification {

    public String id;
    public String notificationTitle;
    public String notificationMessage;
    public long dateCreated;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return notificationTitle;
    }

    public String getMessage() {
        return notificationMessage;
    }

    public String getTimestamp() {
        return getDate(dateCreated);
    }

    public static String getDate(long milliSeconds)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss, dd-MMM-yyyy");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public BoNotification() {
        // Default constructor required for calls to DataSnapshot.getValue(BoNotification.class)
    }
}
