package com.adcoretechnologies.buzuby.notification;

import android.util.Log;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 17/12/16.
 */

public class PushNotificationRegistration extends FirebaseInstanceIdService {

    private IAppService methods;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        log("Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String pushId) {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();
        String userId = Common.getUserId(getApplicationContext());
        String imei = Common.getImei(getApplicationContext());

        if (userId.isEmpty() || userId.equals("0")) {
            log("user id is not available. Aborting push id updation");
            return;
        }

        methods.updatePushId(Const.ACTION_UPDATE_PUSH_ID, imei, userId, pushId, new Callback<PojoPush>() {
            @Override
            public void success(PojoPush pojoPush, Response response) {
                int statusCode = pojoPush.getStatus();
                if (statusCode == Const.STATUS_SUCCESS) {
                    log("Devide push id registered successfully");
                    Toast.makeText(getApplicationContext(), "Device push id updated", Toast.LENGTH_LONG).show();
                } else {
                    log("Devide push id reg failed");
                    Toast.makeText(getApplicationContext(), "Device push id updation failed: " + pojoPush.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while updating push id", error);
            }
        });

    }

    private void log(String s) {
        Log.d(Const.DEBUG_TAG, s);
    }
}
