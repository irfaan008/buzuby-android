package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.adapter.AdapterSpinnerListBased;
import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.pojo.BoCategory;
import com.adcoretechnologies.buzuby.pojo.PojoCategory;
import com.adcoretechnologies.buzuby.util.Const;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 07/01/16.
 */
public class SearchActivity extends BaseActivity {

    ProgressDialog dialog;

    @BindView(R.id.etKeywords)
    EditText etSearchString;
    @BindView(R.id.spCategory)
    Spinner spCategory;
    @BindView(R.id.spSubCategory)
    Spinner spSubCategory;
    @BindView(R.id.spSubSubCategory)
    Spinner spSubSubCategory;
    List<BoCategory> allCategory = null;
    List<BoCategory> allSubCategory = null;
    List<BoCategory> allSubSubCategory = null;
    AdapterSpinnerListBased adapterCategory;
    AdapterSpinnerListBased adapterSubCategory;
    AdapterSpinnerListBased adapterSubSubCategory;
    IAppService methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);

        initViews();

        getAllItems(Const.ACTION_GET_All_CATEGORY, "0");
    }

    private void initViews() {

        if (Const.DEBUG_TEST)
            etSearchString.setText("Toyota");

        dialog = new ProgressDialog(this);

        allCategory = new ArrayList<BoCategory>();
        allSubCategory = new ArrayList<BoCategory>();
        allSubSubCategory = new ArrayList<BoCategory>();

        adapterCategory = new AdapterSpinnerListBased((BaseActivity) this, allCategory);
        adapterSubCategory = new AdapterSpinnerListBased((BaseActivity) this, allSubCategory);
        adapterSubSubCategory = new AdapterSpinnerListBased((BaseActivity) this, allSubSubCategory);

        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemId = allCategory.get(position).getItemId();
                if (itemId.equals("0"))
                    return;
                getAllItems(Const.ACTION_GET_All_SUB_CATEGORY, itemId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemId = allSubCategory.get(position).getItemId();
                if (itemId.equals("0"))
                    return;
                getAllItems(Const.ACTION_GET_All_SUB_SUB_CATEGORY, itemId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spCategory.setAdapter(adapterCategory);
        spSubCategory.setAdapter(adapterSubCategory);
        spSubSubCategory.setAdapter(adapterSubSubCategory);


        resetCollection(Const.ACTION_GET_All_CATEGORY);
        resetCollection(Const.ACTION_GET_All_SUB_CATEGORY);
        resetCollection(Const.ACTION_GET_All_SUB_SUB_CATEGORY);

    }

    private void resetCollection(String itemType) {

        BoCategory item = new BoCategory();
        item.setItemId("0");
        item.setItemName("All");

        if (itemType.equals(Const.ACTION_GET_All_CATEGORY)) {
            allCategory.clear();
            allSubCategory.clear();
            allSubSubCategory.clear();
            allCategory.add(item);
            allSubCategory.add(item);
            allSubSubCategory.add(item);
            spSubCategory.setSelection(0);
            spSubSubCategory.setSelection(0);
        } else if (itemType.equals(Const.ACTION_GET_All_SUB_CATEGORY)) {
            allSubCategory.clear();
            allSubSubCategory.clear();
            allSubCategory.add(item);
            allSubSubCategory.add(item);
            spSubCategory.setSelection(0);
            spSubSubCategory.setSelection(0);
        } else if (itemType.equals(Const.ACTION_GET_All_SUB_SUB_CATEGORY)) {
            allSubSubCategory.clear();
            allSubSubCategory.add(item);
            spSubSubCategory.setSelection(0);
        }

    }

    private void getAllItems(final String action, final String subId) {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Loading items");
        dialog.show();

        methods.getAllCategory(action, subId, new Callback<PojoCategory>() {
            @Override
            public void success(PojoCategory pojoCategory, Response response) {
                BindViewItems(action, pojoCategory.getAllCategory());
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                log("Retrofit error:" + error.getLocalizedMessage());
                Toast.makeText(getApplicationContext(), "Error while retrieving data for : " + subId, Toast.LENGTH_SHORT).show();
                if (dialog != null)
                    dialog.dismiss();

            }
        });

    }


    protected void BindViewItems(String action, List<BoCategory> allItems) {

        if (action.equals(Const.ACTION_GET_All_CATEGORY)) {
            resetCollection(Const.ACTION_GET_All_CATEGORY);
            allCategory.addAll(allItems);
            adapterCategory.notifyDataSetChanged();
        } else if (action.equals(Const.ACTION_GET_All_SUB_CATEGORY)) {
            resetCollection(Const.ACTION_GET_All_SUB_CATEGORY);
            allSubCategory.addAll(allItems);
            adapterSubCategory.notifyDataSetChanged();
        } else if (action.equals(Const.ACTION_GET_All_SUB_SUB_CATEGORY)) {
            resetCollection(Const.ACTION_GET_All_SUB_SUB_CATEGORY);
            allSubSubCategory.addAll(allItems);
            adapterSubSubCategory.notifyDataSetChanged();
        }

    }


    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);
    }


    @OnClick(R.id.tvNext)
    public void NextClick() {

        String searchString = etSearchString.getText().toString().trim();
        if (searchString.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please input search parameter \n ex: kfc, toyota", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, BrowseBusinessActivity.class);
        intent.putExtra(Const.EXTRA_SEARCH_STRING, searchString);
        startActivity(intent);


    }
}
