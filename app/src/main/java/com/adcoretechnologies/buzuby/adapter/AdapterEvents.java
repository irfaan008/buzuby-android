package com.adcoretechnologies.buzuby.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.adcoretechnologies.buzuby.BaseActivity;
import com.adcoretechnologies.buzuby.InbuiltBrowserActivity;
import com.adcoretechnologies.buzuby.R;
import com.adcoretechnologies.buzuby.customviews.FontableTextView;
import com.adcoretechnologies.buzuby.pojo.BoEvent;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterEvents extends
        RecyclerView.Adapter<AdapterEvents.ViewHolder> {
    BaseActivity activity;
    Context context;
    private List<BoEvent> allItems;

    public AdapterEvents(BaseActivity activity, List<BoEvent> allItems) {
        this.allItems = allItems;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.item_deal_events,
                parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final BoEvent item = getItem(position);

        holder.ftvBusinessName.setText(item.getBusinessName());
        holder.ftvDescription.setText(item.getDealHeading());
        holder.ftvPrice.setText(item.getPriceRangeFrom() + "-" + item.getPriceRangeTo() + " " + item.getCurrencySymbol());
        holder.ftvVisitUs.setText(item.getBusinessWebsiteUrl());
        Common.showBannerImage(context, holder.ivEventImage, item.getDealImageurl());
//        log("Logo url : " + item.getBusinessLogoUrl());
        Common.showBusinessLogo(context, holder.ivBusinessLogo, item.getBusinessLogoUrl());

        holder.ftvVisitUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Uri uri = Uri.parse(getItem(position).getBusinessWebsiteUrl()); // missing 'http://' will cause crashed
                Intent intent = new Intent(context, InbuiltBrowserActivity.class);
                intent.putExtra(Const.EXTRA_BUSINESS_NAME, item.getBusinessName());
                intent.putExtra(Const.EXTRA_BUSINESS_URL, item.getBusinessWebsiteUrl());
                context.startActivity(intent);
            }
        });

        holder.ftvLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f", Double.parseDouble(item.getLatitude()), Double.parseDouble(item.getLongitude()));
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return allItems.size();
    }

    public BoEvent getItem(int position) {
        return allItems.get(position);
    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ftvBusinessName)
        FontableTextView ftvBusinessName;
        @BindView(R.id.ftvDescription)
        FontableTextView ftvDescription;
        @BindView(R.id.ftvLocate)
        FontableTextView ftvLocate;
        @BindView(R.id.ftvPrice)
        FontableTextView ftvPrice;
        @BindView(R.id.ftvVisitUs)
        FontableTextView ftvVisitUs;
        @BindView(R.id.ivBusinessLogo)
        ImageView ivBusinessLogo;
        @BindView(R.id.ivEventImage)
        ImageView ivEventImage;

        public ViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);
        }
    }
}
