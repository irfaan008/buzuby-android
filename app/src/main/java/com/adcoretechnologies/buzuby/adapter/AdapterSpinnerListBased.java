package com.adcoretechnologies.buzuby.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.adcoretechnologies.buzuby.BaseActivity;
import com.adcoretechnologies.buzuby.R;
import com.adcoretechnologies.buzuby.pojo.BoCategory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Irfan on 09/01/16.
 */
public class AdapterSpinnerListBased extends ArrayAdapter {

    BaseActivity activity;
    List<BoCategory> allItem;

    @BindView(R.id.tvId)
    TextView tvId;
    @BindView(R.id.tvItem)
    TextView tvItem;


    public AdapterSpinnerListBased(BaseActivity activity, List<BoCategory> allItem) {
        super(activity, 0, allItem);
        this.activity = activity;
        this.allItem = allItem;
    }

    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {

        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.item_spinner, parent, false);

        ButterKnife.bind(this, layout);

        if (allItem == null || allItem.size() == 0) {
            tvItem.setText("Not Available");
            tvId.setText("0");
        } else {
            tvItem.setText(allItem.get(position).getItemName());
            tvId.setText(allItem.get(position).getItemId());
        }
        return layout;
    }

    // It gets a View that displays in the drop down popup the data at the specified position
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // It gets a View that displays the data at the specified position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}
