package com.adcoretechnologies.buzuby.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.adcoretechnologies.buzuby.BaseActivity;
import com.adcoretechnologies.buzuby.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Irfan on 09/01/16.
 */
public class AdapterSpinnerArrayBased extends ArrayAdapter {

    BaseActivity activity;
    String[] allItem;

    @BindView(R.id.tvItem)
    TextView tvItem;


    public AdapterSpinnerArrayBased(BaseActivity activity, String[] allItem) {
        super(activity, 0, allItem);
        this.activity = activity;
        this.allItem = allItem;
    }

    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {

        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.item_spinner, parent, false);

        ButterKnife.bind(this, layout);

        tvItem.setText(allItem[position]);

        return layout;
    }

    // It gets a View that displays in the drop down popup the data at the specified position
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // It gets a View that displays the data at the specified position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}
