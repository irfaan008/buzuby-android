package com.adcoretechnologies.buzuby.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.BaseActivity;
import com.adcoretechnologies.buzuby.BrowseEventsActivity;
import com.adcoretechnologies.buzuby.BusinessDetailActivity;
import com.adcoretechnologies.buzuby.DialogRating;
import com.adcoretechnologies.buzuby.R;
import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.customviews.FontableTextView;
import com.adcoretechnologies.buzuby.pojo.BoBusiness;
import com.adcoretechnologies.buzuby.pojo.PojoCommon;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class AdapterBusiness extends
        RecyclerView.Adapter<AdapterBusiness.ViewHolder> {
    BaseActivity activity;
    Context context;
    DialogRating rating;
    IAppService methods;
    private List<BoBusiness> allItems;
    private ProgressDialog dialog;

    public AdapterBusiness(BaseActivity activity, List<BoBusiness> allItems) {
        this.allItems = allItems;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.item_business,
                parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final BoBusiness item = getItem(position);

        holder.ftvBusinessName.setText(item.getBusinessName());
        holder.ftvDescription.setText(item.getShortDescription());
        holder.ftvPrice.setText("Price Range " + item.getPriceRangeFrom() + "-" + item.getPriceRangeTo() + " " + item.getCurrencySymbol());

        updateFavoriteView(item.isFavorite(), holder.ftvAddFavorite);
        Common.showBannerImage(context, holder.ivBusinessLogo, item.getBusinessLogoUrl());

        holder.ftvDealsAndEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEventsActivity(item.getBusinessId(), item.getBusinessName());
            }
        });

        holder.ivLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f", Double.parseDouble(item.getLatitude()), Double.parseDouble(item.getLongitude()));
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                context.startActivity(intent);
            }
        });

        holder.ftvAddFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                EventBus.getDefault().post(new BOEventData(Const.EVENT_FAVORITE, item.getBusinessId(), null));
                addBusinessToFavoriteList(item.getBusinessId(), holder.ftvAddFavorite);
            }
        });

        holder.ratingBar.setRating(Float.parseFloat(item.getRating()));

        holder.llHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BusinessDetailActivity.class);
                intent.putExtra(Const.EXTRA_BUSINESS_ID, item.getBusinessId());
                intent.putExtra(Const.EXTRA_BUSINESS_NAME, item.getBusinessName());
                context.startActivity(intent);
            }
        });

        holder.ftvDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BusinessDetailActivity.class);
                intent.putExtra(Const.EXTRA_BUSINESS_ID, item.getBusinessId());
                intent.putExtra(Const.EXTRA_BUSINESS_NAME, item.getBusinessName());
                context.startActivity(intent);
            }
        });

        holder.ratingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    RatingClick(item.getBusinessId(), item.getBusinessName(), item.getRatingProvidedByUser());
                }
                return true;
            }
        });

        holder.ftvBusinessDay.setText("Today");
        holder.ftvBusinessTime.setText(item.getTodayTimings());

    }

    public void RatingClick(String businessId, String businessName, String myRating) {
        if (rating == null) {
            rating = DialogRating.newInstance(businessId, myRating, businessName);
        }
        rating.show(activity.getSupportFragmentManager(), "dialogRating");
    }

    private void openEventsActivity(String businessId, String businessName) {
        Intent intent = new Intent(activity, BrowseEventsActivity.class);
        intent.putExtra(Const.EXTRA_BUSINESS_ID, businessId);
        intent.putExtra(Const.EXTRA_BUSINESS_NAME, businessName);
        activity.startActivity(intent);
    }

    private void updateFavoriteView(boolean isFavorite, FontableTextView ftvAddFavorite) {
        if (isFavorite)
            ftvAddFavorite.setText("Remove from favorite");
        else
            ftvAddFavorite.setText("Add to favorite");
    }

    private void addBusinessToFavoriteList(String businessId, final FontableTextView ftvAddFavorite) {
        dialog = new ProgressDialog(activity);
        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Updating favorite list");
        dialog.show();

        methods.addBusinessToFavorite(Const.ACTION_ADD_BUSINESS_TO_FAVORITE, Common.getToken(context), Common.getUserId(context), businessId, new Callback<PojoCommon>() {
            @Override
            public void success(PojoCommon pojo, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojo.getStatus();
                String message = pojo.getMessage();
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                if (code == Const.STATUS_SUCCESS) {
                    updateFavoriteView(message.contains("added"), ftvAddFavorite);


                } else if (code == Const.STATUS_ERROR)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();

            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while performing favorite ", error);

                if (dialog != null)
                    dialog.dismiss();

            }
        });


    }

    @Override
    public int getItemCount() {
        return allItems.size();
    }

    public BoBusiness getItem(int position) {
        return allItems.get(position);
    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);

    }

    // Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ftvBusinessName)
        FontableTextView ftvBusinessName;
        @BindView(R.id.ftvDescription)
        FontableTextView ftvDescription;
        @BindView(R.id.ivLocation)
        ImageView ivLocate;
        @BindView(R.id.ftvDealsAndEvents)
        FontableTextView ftvDealsAndEvents;
        @BindView(R.id.ftvAddFavorite)
        FontableTextView ftvAddFavorite;
        @BindView(R.id.ftvBusinessDay)
        FontableTextView ftvBusinessDay;
        @BindView(R.id.ftvBusinessTime)
        FontableTextView ftvBusinessTime;
        @BindView(R.id.ftvPrice)
        FontableTextView ftvPrice;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;

        @BindView(R.id.ivBusinessLogo)
        ImageView ivBusinessLogo;

        @BindView(R.id.llBusinessHeader)
        LinearLayout llHeader;


        public ViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);
        }

    }
}
