package com.adcoretechnologies.buzuby;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.util.Const;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Irfan on 07/01/16.
 */
public class InbuiltBrowserActivity extends BaseActivity {

    @BindView(R.id.wvContent)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(Const.EXTRA_BUSINESS_NAME)) {
                String businessName = intent.getExtras().getString(Const.EXTRA_BUSINESS_NAME);
                ab.setTitle(businessName);

                if (intent.hasExtra(Const.EXTRA_BUSINESS_URL)) {
                    String businessURl = intent.getExtras().getString(Const.EXTRA_BUSINESS_URL);

                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setAppCacheEnabled(true);
                    webView.getSettings().setBuiltInZoomControls(true);
                    webView.setWebViewClient(new WebViewClient());
                    webView.loadUrl(businessURl);
                }
            }
        } else
            Toast.makeText(getApplicationContext(), "The business has provided incorrect website address", Toast.LENGTH_LONG).show();

    }


//    @OnClick(R.id.tvNext)
//    public void NextClick() {
//        // TODO show List of events
//
//    }
}
