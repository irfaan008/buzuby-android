package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.adapter.AdapterBusiness;
import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.pojo.BOEventData;
import com.adcoretechnologies.buzuby.pojo.BoBusiness;
import com.adcoretechnologies.buzuby.pojo.PojoBusiness;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 07/01/16.
 */
public class BrowseBusinessActivity extends BaseActivity {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    AdapterBusiness adapterBusiness;
    IAppService methods;
    String searchString = "";
    private ProgressDialog dialog;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_business);

        ButterKnife.bind(this);

        dialog = new ProgressDialog(this);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        if (searchString.isEmpty()) {
            Intent intent = getIntent();
            searchString = intent.getExtras().getString(Const.EXTRA_SEARCH_STRING);
        }

        getBusiness(searchString, "0", "0", "0");
    }

    private void getBusiness(String keyword, String categoryId, String subCategoryId, String subSubCategoryId) {

        if (Const.DEBUG_TEST) {
            categoryId = "0";
            subCategoryId = "0";
            subSubCategoryId = "0";

        }

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Getting Business & Deals based on your preference");
        dialog.show();

        methods.getBusinessByPreference(Const.ACTION_GET_ALL_BUSINESS_BY_PREFERENCE, Common.getToken(getApplicationContext()), Common.getUserId(getApplicationContext()), keyword, categoryId, subCategoryId, subSubCategoryId, new Callback<PojoBusiness>() {
            @Override
            public void success(PojoBusiness pojo, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojo.getStatus();
                String message = pojo.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    BindBusiness(pojo.getAllBusinesss());
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while getting Business based on preference", error);

                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }

    private void BindBusiness(ArrayList<BoBusiness> allBusiness) {

        adapterBusiness = new AdapterBusiness(this, allBusiness);
        mRecyclerView.setAdapter(adapterBusiness);

    }

    public void onEventMainThread(BOEventData eventData) {
        int eventType = eventData.eventType;

    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
