package com.adcoretechnologies.buzuby;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.adcoretechnologies.buzuby.pojo.BoUser;
import com.adcoretechnologies.buzuby.util.Common;

/**
 * Created by Irfan on 07/01/16.
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

//        if (Const.DEBUG_TEST) {
//            openMainActivity();
//            return;
//        }

        BoUser user = Common.getUserDetail(getApplicationContext());

        final Intent intent;

        if (user == null || user.getUserId().isEmpty() || user.getAuthToken().isEmpty() || user.getFullName().isEmpty() || user.getEmail().isEmpty())
            intent = new Intent(this, LoginActivity.class);
        else
            intent = new Intent(this, MainActivity.class);

        Thread background = new Thread() {
            public void run() {

                try {
                    sleep(2000 * 1);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {
                }
            }
        };
        background.start();
    }
}
