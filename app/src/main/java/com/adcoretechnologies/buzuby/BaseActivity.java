package com.adcoretechnologies.buzuby;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Irfan on 07/01/16.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
