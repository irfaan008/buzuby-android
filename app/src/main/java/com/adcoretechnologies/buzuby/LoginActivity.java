package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.pojo.BoUser;
import com.adcoretechnologies.buzuby.pojo.PojoUser;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;
import com.adcoretechnologies.buzuby.util.Pref;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 07/01/16.
 */
public class LoginActivity extends BaseActivity {

    @BindView(R.id.tvRegister)
    TextView tvRegister;
    @BindView(R.id.tvForgot)
    TextView tvForgot;
    @BindView(R.id.tvLogin)
    TextView tvLogin;
    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etPassword)
    EditText etPassword;

    ProgressDialog dialog;
    IAppService methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        dialog = new ProgressDialog(this);

        configureTest();
    }

    private void configureTest() {
        if (Const.DEBUG_TEST) {
            etUsername.setText("test1");
            etPassword.setText("test1");
        }
    }

    @OnClick(R.id.tvRegister)
    public void registerClick() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvForgot)
    public void ForgotPasswordClick() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvLogin)
    public void LoginClick() {
        String userName = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (validateInput(userName, password))
            performLogin(userName, password);
        else
            Toast.makeText(getApplicationContext(), "Please provide all input correctly", Toast.LENGTH_LONG).show();
    }

    private boolean validateInput(String userName, String password) {

        if (userName.isEmpty() || password.isEmpty())
            return false;
        else
            return true;
    }

    private void performLogin(String username, String password) {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Authenticating user");
        dialog.show();

        String imei = Common.getImei(getApplicationContext());
        String token = FirebaseInstanceId.getInstance().getToken();

        methods.performLogin(Const.ACTION_DO_LOGIN, username, password, "android", imei, token, new Callback<PojoUser>() {
            @Override
            public void success(PojoUser pojoUser, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojoUser.getStatus();
                String message = pojoUser.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    BindViewItems(pojoUser.getAllUser().get(0));
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while logging in", error);

                if (dialog != null)
                    dialog.dismiss();

            }
        });

    }

    private void BindViewItems(BoUser user) {

        if (user != null) {
            Pref.Write(getApplicationContext(), Const.PREF_USER_ID, user.getUserId());
            Pref.Write(getApplicationContext(), Const.PREF_FULL_NAME, user.getFirstName() + " " + user.getLastName());
            Pref.Write(getApplicationContext(), Const.PREF_AUTH_TOKEN, user.getAuthToken());
            Pref.Write(getApplicationContext(), Const.PREF_ADDRESS, user.getAddress() + ", " + user.getCity() + ", " + user.getSuburb() + ", " + user.getProvince() + ", " + user.getCountry());
            Pref.Write(getApplicationContext(), Const.PREF_DOB, user.getDOB());
//            Pref.Write(getApplicationContext(), Const.PREF_LATITUDE, user.getLatitude());
            Pref.Write(getApplicationContext(), Const.PREF_EMAIL, user.getEmail());
//            Pref.Write(getApplicationContext(), Const.PREF_LONGITUDE, user.getLongitude());
            Pref.Write(getApplicationContext(), Const.PREF_MOBILENUMBER, user.getMobileNumber());

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else
            Toast.makeText(getApplicationContext(), "Can't login at this time. Please connect with system admin", Toast.LENGTH_LONG).show();

    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);
    }
}
