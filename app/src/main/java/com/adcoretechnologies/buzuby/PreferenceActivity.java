package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.adapter.AdapterSpinnerArrayBased;
import com.adcoretechnologies.buzuby.adapter.AdapterSpinnerListBased;
import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.pojo.BoCategory;
import com.adcoretechnologies.buzuby.pojo.PojoCategory;
import com.adcoretechnologies.buzuby.service.GPSTracker;
import com.adcoretechnologies.buzuby.util.Const;
import com.adcoretechnologies.buzuby.util.Pref;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 07/01/16.
 */
public class PreferenceActivity extends BaseActivity {

    @BindView(R.id.txtGPSLocation)
    TextView txtGPSLocation;
    @BindView(R.id.spRadius)
    Spinner spRadius;
    @BindView(R.id.spCategory)
    Spinner spCategory;
    @BindView(R.id.spSubCategory)
    Spinner spSubCategory;
    @BindView(R.id.spSubSubCategory)
    Spinner spSubSubCategory;
    @BindView(R.id.spCurrencyType)
    Spinner spCurrencyType;
    @BindView(R.id.etPriceEnd)
    EditText etPriceEnd;
    @BindView(R.id.etPriceStart)
    EditText etPriceStart;
    @BindView(R.id.tvLatLong)
    TextView tvLocation;
    double latitude = 0.0;
    double longitude = 0.0;
    List<BoCategory> allCategory = null;
    List<BoCategory> allSubCategory = null;
    List<BoCategory> allSubSubCategory = null;
    AdapterSpinnerListBased adapterCategory;
    AdapterSpinnerListBased adapterSubCategory;
    AdapterSpinnerListBased adapterSubSubCategory;
    AdapterSpinnerArrayBased adapterRadius;
    String[] radius = {"5", "10", "15", "20", "50", "100"};
    AdapterSpinnerArrayBased adapterCurrencyType;
    String[] currencyTypes = {"dollar", "euro", "rand"};
    IAppService methods;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);

        ButterKnife.bind(this);

        initViews();

        getAllItems(Const.ACTION_GET_All_CATEGORY, "0");
    }

    private void initViews() {

        dialog = new ProgressDialog(this);

        allCategory = new ArrayList<BoCategory>();
        allSubCategory = new ArrayList<BoCategory>();
        allSubSubCategory = new ArrayList<BoCategory>();

        adapterCategory = new AdapterSpinnerListBased((BaseActivity) this, allCategory);
        adapterSubCategory = new AdapterSpinnerListBased((BaseActivity) this, allSubCategory);
        adapterSubSubCategory = new AdapterSpinnerListBased((BaseActivity) this, allSubSubCategory);
        adapterRadius = new AdapterSpinnerArrayBased((BaseActivity) this, radius);
        adapterCurrencyType = new AdapterSpinnerArrayBased((BaseActivity) this, currencyTypes);

        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemId = allCategory.get(position).getItemId();
                if (itemId.equals("0"))
                    return;
                getAllItems(Const.ACTION_GET_All_SUB_CATEGORY, itemId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemId = allSubCategory.get(position).getItemId();
                if (itemId.equals("0"))
                    return;
                getAllItems(Const.ACTION_GET_All_SUB_SUB_CATEGORY, itemId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spCategory.setAdapter(adapterCategory);
        spSubCategory.setAdapter(adapterSubCategory);
        spSubSubCategory.setAdapter(adapterSubSubCategory);
        spRadius.setAdapter(adapterRadius);
        spCurrencyType.setAdapter(adapterCurrencyType);

        resetCollection(Const.ACTION_GET_All_CATEGORY);
        resetCollection(Const.ACTION_GET_All_SUB_CATEGORY);
        resetCollection(Const.ACTION_GET_All_SUB_SUB_CATEGORY);

      //  tvLocation.setText(latitude + ", " + longitude);

        txtGPSLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                latitude= new GPSTracker(PreferenceActivity.this).getLatitude();
                longitude= new GPSTracker(PreferenceActivity.this).getLongitude();
                tvLocation.setText(latitude + ", " + longitude);
            }
        });
    }

    private void resetCollection(String itemType) {

        BoCategory item = new BoCategory();
        item.setItemId("0");
        item.setItemName("All");

        if (itemType.equals(Const.ACTION_GET_All_CATEGORY)) {
            allCategory.clear();
            allSubCategory.clear();
            allSubSubCategory.clear();
            allCategory.add(item);
            allSubCategory.add(item);
            allSubSubCategory.add(item);
            spSubCategory.setSelection(0);
            spSubSubCategory.setSelection(0);
        } else if (itemType.equals(Const.ACTION_GET_All_SUB_CATEGORY)) {
            allSubCategory.clear();
            allSubSubCategory.clear();
            allSubCategory.add(item);
            allSubSubCategory.add(item);
            spSubCategory.setSelection(0);
            spSubSubCategory.setSelection(0);
        } else if (itemType.equals(Const.ACTION_GET_All_SUB_SUB_CATEGORY)) {
            allSubSubCategory.clear();
            allSubSubCategory.add(item);
            spSubSubCategory.setSelection(0);
        }

    }

    private void getAllItems(final String action, final String subId) {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Loading items");
        dialog.show();

        methods.getAllCategory(action, subId, new Callback<PojoCategory>() {
            @Override
            public void success(PojoCategory pojoCategory, Response response) {
                BindViewItems(action, pojoCategory.getAllCategory());
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                log("Retrofit error:" + error.getLocalizedMessage());
                Toast.makeText(getApplicationContext(), "Error while retrieving data for : " + subId, Toast.LENGTH_SHORT).show();
                if (dialog != null)
                    dialog.dismiss();

            }
        });

    }


    protected void BindViewItems(String action, List<BoCategory> allItems) {

        if (action.equals(Const.ACTION_GET_All_CATEGORY)) {
            resetCollection(Const.ACTION_GET_All_CATEGORY);
            allCategory.addAll(allItems);
            adapterCategory.notifyDataSetChanged();
        } else if (action.equals(Const.ACTION_GET_All_SUB_CATEGORY)) {
            resetCollection(Const.ACTION_GET_All_SUB_CATEGORY);
            allSubCategory.addAll(allItems);
            adapterSubCategory.notifyDataSetChanged();
        } else if (action.equals(Const.ACTION_GET_All_SUB_SUB_CATEGORY)) {
            resetCollection(Const.ACTION_GET_All_SUB_SUB_CATEGORY);
            allSubSubCategory.addAll(allItems);
            adapterSubSubCategory.notifyDataSetChanged();
        }

    }


    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);
    }

    @OnClick(R.id.tvNext)
    public void NextClick() {
        String startPrice = etPriceStart.getText().toString().trim();
        String endPrice = etPriceEnd.getText().toString().trim();
        boolean isValidated = validateInputs(startPrice, endPrice);

        if (isValidated) {
            //  Save preference and redirected to next screen

            Pref.Write(getApplicationContext(), Const.PREF_LATITUDE, latitude + "");
            Pref.Write(getApplicationContext(), Const.PREF_LONGITUDE, longitude + "");
            Pref.Write(getApplicationContext(), Const.PREF_RADIUS, spRadius.getSelectedItem().toString());
            Pref.Write(getApplicationContext(), Const.PREF_CATEGORYID, allCategory.get(spCategory.getSelectedItemPosition()).getItemId());

            int subCatPosition = spSubCategory.getSelectedItemPosition();
            if (subCatPosition == -1)
                subCatPosition = 0;
            String subCateId = allSubCategory.get(subCatPosition).getItemId();
            Pref.Write(getApplicationContext(), Const.PREF_SUBCATEGORYID, subCateId);

            int subSubCatPosition = spSubSubCategory.getSelectedItemPosition();
            if (subSubCatPosition == -1)
                subSubCatPosition = 0;
            Pref.Write(getApplicationContext(), Const.PREF_SUBSUBCATEGORYID, allSubSubCategory.get(subSubCatPosition).getItemId());
            Pref.Write(getApplicationContext(), Const.PREF_START_PRICE, startPrice);
            Pref.Write(getApplicationContext(), Const.PREF_END_PRICE, endPrice);
            Pref.Write(getApplicationContext(), Const.PREF_CURRENCY_TYPE, spCurrencyType.getSelectedItem().toString());


        } else {
            return;
        }
        Intent intent = new Intent(this, BrowseEventsActivity.class);
        startActivity(intent);
        finish();

    }

    private boolean validateInputs(String startPrice, String endPrice) {
        if (latitude == 0.0 || longitude == 0.0) {
            Toast.makeText(getApplicationContext(), "Your location is not determined. Please check location setting and try again", Toast.LENGTH_LONG).show();
            return false;
        } else if (startPrice.isEmpty() || endPrice.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please input the correct price range", Toast.LENGTH_LONG).show();
            return false;
        }
        else return true;
    }
}
