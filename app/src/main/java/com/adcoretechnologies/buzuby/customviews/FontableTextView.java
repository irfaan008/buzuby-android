package com.adcoretechnologies.buzuby.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.adcoretechnologies.buzuby.R;


public class FontableTextView extends TextView {

    public FontableTextView(Context context) {
        super(context);
    }

    public FontableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_buzuby_customviews_FontableTextView,
                R.styleable.com_buzuby_customviews_FontableTextView_font);
    }

    public FontableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_buzuby_customviews_FontableTextView,
                R.styleable.com_buzuby_customviews_FontableTextView_font);
    }
}
