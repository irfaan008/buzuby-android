package com.adcoretechnologies.buzuby.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.adcoretechnologies.buzuby.R;


public class FontableEditTextView extends EditText {

    public FontableEditTextView(Context context) {
        super(context);
    }

    public FontableEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_buzuby_customviews_FontableEditTextView,
                R.styleable.com_buzuby_customviews_FontableEditTextView_font);
    }

    public FontableEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_buzuby_customviews_FontableEditTextView,
                R.styleable.com_buzuby_customviews_FontableEditTextView_font);
    }
}
