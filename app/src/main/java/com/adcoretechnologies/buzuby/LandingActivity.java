package com.adcoretechnologies.buzuby;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Irfan on 07/01/16.
 */
public class LandingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
        finish();
    }
}
