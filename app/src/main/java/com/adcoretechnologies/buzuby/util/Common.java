package com.adcoretechnologies.buzuby.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;

import com.adcoretechnologies.buzuby.R;
import com.adcoretechnologies.buzuby.pojo.BoUser;
import com.squareup.picasso.Picasso;

import retrofit.RetrofitError;

/**
 * Created by Irfan on 10/01/16.
 */
public class Common {
    public static void printException(String message, RetrofitError error) {
        log("My message: " + message);
        log("Retrofit cause: " + error.getCause());
        log("Retrofit error:" + error.getLocalizedMessage());
        log("Retrofit stacktrace: " + error.getStackTrace());

    }

    public static void printException(String message, Exception error) {
        log("My message: " + message);
        log("Retrofit cause: " + error.getCause());
        log("Retrofit error:" + error.getLocalizedMessage());
        log("Retrofit stacktrace: " + error.getStackTrace());

    }

    public static void showBannerImage(Context context, ImageView imageView, String url) {
        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.banner_sample)
//                            .resize(300,110)
                    .error(R.drawable.banner_sample)
                    .into(imageView);
        } catch (Exception ex) {
            printException("Unable to load banner", ex);
        }
    }

    public static void showBusinessLogo(Context context, ImageView imageView, String url) {
        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.mipmap.ic_launcher)
                    //.resize(380, 380)
                    .error(R.mipmap.ic_launcher)
                    .into(imageView);
        } catch (Exception ex) {
            printException("Unable to load logo", ex);
        }
    }

    public static void showBusinessLogoThumb(Context context, ImageView imageView, String url) {
        try {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.mipmap.ic_launcher)

                    //         .resize(90, 90)
                    .error(R.mipmap.ic_launcher)
                    .into(imageView);
        } catch (Exception ex) {
            printException("Unable to load logo", ex);
        }
    }

    private static void log(String message) {
        Log.d(Const.DEBUG_TAG, message);
    }

    public static String getToken(Context context) {
        return Pref.Read(context, Const.PREF_AUTH_TOKEN);
    }

    public static String getUserId(Context context) {
        return Pref.Read(context, Const.PREF_USER_ID);

    }

    public static BoUser getUserDetail(Context context) {

        BoUser user = new BoUser();
        user.setAuthToken(Pref.Read(context, Const.PREF_AUTH_TOKEN));
        user.setUserId(Pref.Read(context, Const.PREF_USER_ID));
        user.setEmail(Pref.Read(context, Const.PREF_EMAIL));
        user.setFullName(Pref.Read(context, Const.PREF_FULL_NAME));
        return user;
    }

    public static void setUserDetail(Context context, BoUser user) {

        Pref.Write(context, Const.PREF_EMAIL, user.getEmail());
        Pref.Write(context, Const.PREF_USER_ID, user.getUserId());
        Pref.Write(context, Const.PREF_AUTH_TOKEN, user.getAuthToken());
        Pref.Write(context, Const.PREF_FULL_NAME, user.getFullName());
    }

    public static String getImei(Context context) {
        TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return mngr.getDeviceId();
    }
}
