package com.adcoretechnologies.buzuby.util;

/**
 * Created by Irfan on 11/09/15.
 */
public class Const {
    public static final String DEBUG_TAG = "bzby";
    public static final boolean IS_PRODUCTION = true;
    public static final boolean DEBUG_TEST = true;
    public static final boolean DEBUG_DEVELOPER_MENU = false;


    public static final String API_BASE_URL = "http://buzuby.com/app/webroot/api/";
    public static final String API_BASE_URL_DEV = "http://newprojectdevelopment.com/buzuby/app/webroot/api";

    public static final String ACTION_GET_All_CATEGORY = "getAllCategory";
    public static final String ACTION_GET_All_SUB_CATEGORY = "getSubCategoryByCategoryId";
    public static final String ACTION_GET_All_SUB_SUB_CATEGORY = "getSubSubCategoryBySubCategoryId";
    public static final String ACTION_DO_LOGIN = "login";
    public static final String ACTION_DO_REGISTRATION = "Register";


    public static final String PREF_USER_ID = "userId";
    public static final String PREF_FULL_NAME = "name";
    public static final String PREF_ADDRESS = "address";
    public static final String PREF_AUTH_TOKEN = "authToken";
    public static final String PREF_DOB = "dob";
    public static final String PREF_LATITUDE = "latitude";
    public static final String PREF_EMAIL = "email";
    public static final String PREF_LONGITUDE = "longitude";
    public static final String PREF_MOBILENUMBER = "mobile";
    public static final int STATUS_SUCCESS = 200;
    public static final int STATUS_ERROR = 100;
    public static final int STATUS_EXCEPTION = 300;

    public static final int EVENT_FAVORITE = 1;
    public static final String ACTION_GET_MY_FAVORITE_LIST = "getAllFavoriteBusiness";
    public static final String ACTION_GET_ALL_BANNER = "getAdBanner";
    public static final String PREF_CURRENCY_TYPE = "currencyType";
    public static final String PREF_END_PRICE = "endPrice";
    public static final String PREF_START_PRICE = "startPrice";
    public static final String PREF_SUBSUBCATEGORYID = "subsubcategoryId";
    public static final String PREF_RADIUS = "radius";
    public static final String PREF_CATEGORYID = "categoryId";
    public static final String PREF_SUBCATEGORYID = "subCategoryId";
    public static final String ACTION_GET_ALL_DEAL_EVENTS = "getDealEventsByPreference";
    public static final String ACTION_GET_ALL_BUSINESS_BY_PREFERENCE = "getBusinessByPreference";
    public static final String EXTRA_BUSINESS_NAME = "extraBusinessNmae";
    public static final String EXTRA_BUSINESS_URL = "extraUrl";
    public static final String EXTRA_SEARCH_STRING = "extraSearch";
    public static final String ACTION_ADD_BUSINESS_TO_FAVORITE = "addBusinessToFavorite";
    public static final String EXTRA_BUSINESS_ID = "extraBusinessId";
    public static final String ACTION_GET_BUSINESS_DETAIL_BY_ID = "getBusinessDetailById";
    public static final String ACTION_GET_ALL_BUSINESS_BY_BUSINESS_ID = "getDealEventsByBusinessId";
    public static final String ACTION_GET_ALL_NOTIFICATION = "getUserPushNotification";
    public static final String EXTRA_MY_RATING = "extraRating";
    public static final String ACTION_ADD_RATING_TO_BUSINESS = "AddRatingToBusiness";
    public static final String ACTION_UPDATE_PUSH_ID = "updateDeviceInfo";
    public static final String ACTION_REGISTER_QUICK = "registerV2";
    public static int EVENT_RATING = 2;

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";
    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = "ah_firebase";
}
