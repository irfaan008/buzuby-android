package com.adcoretechnologies.buzuby;

import android.app.ActionBar;
import android.os.Bundle;

/**
 * Created by Irfan on 07/01/16.
 */
public class ForgotPasswordActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ActionBar ab = getActionBar();
        if (ab != null)
            ab.setTitle(R.string.forgot_password);
    }
}
