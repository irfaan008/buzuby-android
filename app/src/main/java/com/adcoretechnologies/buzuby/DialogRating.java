package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.customviews.FontableTextView;
import com.adcoretechnologies.buzuby.pojo.BOEventData;
import com.adcoretechnologies.buzuby.pojo.PojoCommon;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 21/02/16.
 */
public class DialogRating extends DialogFragment {
    View rootView;
    FontableTextView ftvSubmit;
    RatingBar ratingBar;
    IAppService methods;
    private ProgressDialog dialog;

    public DialogRating() {
    }

    public static DialogRating newInstance(String businessId, String myRating, String businessName) {
        DialogRating dialogRating = new DialogRating();
        Bundle args = new Bundle();
        args.putString(Const.EXTRA_BUSINESS_ID, businessId);
        args.putString(Const.EXTRA_BUSINESS_NAME, businessName);
        args.putString(Const.EXTRA_MY_RATING, myRating);
        dialogRating.setArguments(args);
        return dialogRating;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle args = getArguments();
        final String businessId = args.getString(Const.EXTRA_BUSINESS_ID);
        String myRating = args.getString(Const.EXTRA_MY_RATING);
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_rating_dialog,
                    container, false);

            ratingBar = (RatingBar) rootView.findViewById(R.id.ratingBar);
            ratingBar.setRating(Float.parseFloat(myRating));

            ftvSubmit = (FontableTextView) rootView.findViewById(R.id.ftvSubmit);
            ftvSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitRating(businessId, ratingBar.getRating());
                }
            });


        } catch (Exception ex) {
            log("Error inflating dialog rating");
            Common.printException(getClass().getName() + " :OnCreateView", ex);
        }

        return rootView;
    }

    private void submitRating(String businessId, final float rating) {
        dialog = new ProgressDialog(getActivity());
        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Updating business rating");
        dialog.show();

        methods.addBusinessRating(Const.ACTION_ADD_RATING_TO_BUSINESS, Common.getToken(getContext()), Common.getUserId(getContext()), businessId, rating + "", new Callback<PojoCommon>() {
            @Override
            public void success(PojoCommon pojo, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojo.getStatus();
                String message = pojo.getMessage();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                if (code == Const.STATUS_SUCCESS) {
                    getDialog().dismiss();
                    EventBus.getDefault().post(new BOEventData(Const.EVENT_RATING, null, rating + ""));
                } else if (code == Const.STATUS_ERROR) {

                }


            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while saving rating", error);

                if (dialog != null)
                    dialog.dismiss();

            }
        });
    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);

    }
}
