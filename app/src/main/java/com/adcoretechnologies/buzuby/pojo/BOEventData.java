package com.adcoretechnologies.buzuby.pojo;

/**
 * Created by Irfan on 07/10/15.
 */
public class BOEventData {
    public final int eventType;
    public final String Id;
    public final String data;

    public BOEventData(int eventType, String Id, String data) {
        this.eventType = eventType;
        this.data = data;
        this.Id = Id;
    }
}
