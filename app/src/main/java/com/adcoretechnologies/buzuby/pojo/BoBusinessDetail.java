package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;


/**
 * Created by Irfan on 09/01/16.
 */

@Parcel
public class BoBusinessDetail {

    @SerializedName("id")
    public String businessId;

    @SerializedName("name")
    public String businessName;

    @SerializedName("businessImageLogoUrl")
    public String businessLogoUrl;

    @SerializedName("businessImageBannerUrl")
    public String businessImageUrl;

    @SerializedName("ratingProvidedByUser")
    public String ratingProvidedByUser;


    @SerializedName("isFavorite")
    public String isFavorite;

    @SerializedName("currencySymbol")
    public String currencySymbol;

    @SerializedName("rating")
    public String rating;

    @SerializedName("latitude")
    public String latitude;
    @SerializedName("longitude")
    public String longitude;

    @SerializedName("country")
    public String country;
    @SerializedName("currency")
    public String currencyType;
    @SerializedName("province")
    public String province;
    @SerializedName("suburb")
    public String suburb;
    @SerializedName("city")
    public String city;
    @SerializedName("phone")
    public String phone;
    @SerializedName("description")
    public String description;
    @SerializedName("website_link")
    public String businessWebsiteUrl;
    @SerializedName("specialize_in")
    public String specializeIn;
    @SerializedName("facebook_page")
    public String facebookPage;
    @SerializedName("amenities")
    public String amenities;
    @SerializedName("price_range_from")
    public String priceRangeFrom;
    @SerializedName("price_range_to")
    public String priceRangeTo;

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public String getFullAddress() {
        return city + ", " + suburb + ", " + province + ", " + country;
    }

    public String getPriceRange() {
        return "Price Range " + priceRangeFrom + " - " + priceRangeTo + " " + currencyType;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessLogoUrl() {
        return businessLogoUrl;
    }

    public void setBusinessLogoUrl(String businessLogoUrl) {
        this.businessLogoUrl = businessLogoUrl;
    }

    public String getBusinessImageUrl() {
        return businessImageUrl;
    }

    public void setBusinessImageUrl(String businessImageUrl) {
        this.businessImageUrl = businessImageUrl;
    }

    public String getRatingProvidedByUser() {
        return ratingProvidedByUser;
    }

    public void setRatingProvidedByUser(String ratingProvidedByUser) {
        this.ratingProvidedByUser = ratingProvidedByUser;
    }

    public boolean isFavorite() {
        if (isFavorite.toLowerCase().equals("yes"))
            return true;
        else return false;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getRating() {
        return rating == null ? "0.0" : rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBusinessWebsiteUrl() {
        return businessWebsiteUrl;
    }

    public void setBusinessWebsiteUrl(String businessWebsiteUrl) {
        this.businessWebsiteUrl = businessWebsiteUrl;
    }

    public String getSpecializeIn() {
        return specializeIn;
    }

    public void setSpecializeIn(String specializeIn) {
        this.specializeIn = specializeIn;
    }

    public String getFacebookPage() {
        return facebookPage;
    }

    public void setFacebookPage(String facebookPage) {
        this.facebookPage = facebookPage;
    }

    public String getAmenities() {
        String am = "";
        try {
            if (!amenities.isEmpty()) {
                String[] ar = amenities.split(",");
                for (String a : ar) {
                    if (!a.startsWith(" "))
                        a = " " + a;
                    //  List of bullets http://www.elizabethcastro.com/html/extras/entities.html
                    am += "&#187;" + a + "<br/>";
                }
            } else
                am = "No data available";
        } catch (Exception ex) {
            am = "Error while retrieving amenities";
        }
        return am;
    }

    @SerializedName("operatingTime")
    public List<BoTiming> operatingTime;

    public void setOperatingTime(List<BoTiming> operatingTime) {
        this.operatingTime = operatingTime;
    }


    public String getStoreTimings() {
        String timings = "No data available";
        if (operatingTime.size() > 0) {
            timings = "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head><style>body{\n" +
                    "background:#dc8a54;\n" +
                    "color:white;\n" +
                    "}\n" +
                    "table{\n" +
                    "width:100%;\n" +
                    "border-collapse:collapse;\n" +
                    "border:1px solid white;\n" +
                    "}\n" +
                    "th, td{\n" +
                    "border:1px solid white;\ntext-align:center;" +
                    "}" +
                    "</style>" +
                    "</head>\n" +
                    "<body>\n<table>";
            timings += "<tr>";
            for (int i = 0; i < 7; i++) {
                timings += "<th>" + operatingTime.get(i).getDay() + "</th>";
            }
            timings += "</tr>";
            timings += "<tr>";
            for (int i = 0; i < 7; i++) {
                timings += "<td>" + (operatingTime.get(i).getStartTime().equals("null") ? "-" : operatingTime.get(i).getStartTime()) + "</td>";
            }
            timings += "</tr>";
            timings += "<tr>";
            for (int i = 0; i < 7; i++) {
                timings += "<td>" + (operatingTime.get(i).getEndTime().equals("null") ? "-" : operatingTime.get(i).getEndTime()) + "</td>";
            }
            timings += "</tr>";
            timings += "</table></body>\n" +
                    "</html>";
        }

        return timings;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    public String getPriceRangeFrom() {
        return priceRangeFrom;
    }

    public void setPriceRangeFrom(String priceRangeFrom) {
        this.priceRangeFrom = priceRangeFrom;
    }

    public String getPriceRangeTo() {
        return priceRangeTo;
    }

    public void setPriceRangeTo(String priceRangeTo) {
        this.priceRangeTo = priceRangeTo;
    }
}
