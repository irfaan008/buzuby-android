package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;


/**
 * Created by Irfan on 09/01/16.
 */

@Parcel
public class BoBusiness {

    @SerializedName("businessId")
    public String businessId;

    @SerializedName("businessName")
    public String businessName;

    @SerializedName("businessImageLogoUrl")

    public String businessLogoUrl;

    @SerializedName("ratingProvidedByUser")
    public String ratingProvidedByUser;


    @SerializedName("isFavorite")
    public String isFavorite;

    @SerializedName("currencySymbol")
    public String currencySymbol;

    @SerializedName("rating")
    public String rating;

    @SerializedName("priceRangeFrom")
    public String priceRangeFrom;
    @SerializedName("priceRangeTo")
    public String priceRangeTo;

    @SerializedName("latitude")
    public String latitude;
    @SerializedName("longitude")
    public String longitude;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @SerializedName("shortDescription")
    public String shortDescription;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessLogoUrl() {
        return businessLogoUrl;
    }

    public void setBusinessLogoUrl(String businessLogoUrl) {
        this.businessLogoUrl = businessLogoUrl;
    }

    public String getRatingProvidedByUser() {
        return ratingProvidedByUser;
    }

    public void setRatingProvidedByUser(String ratingProvidedByUser) {
        this.ratingProvidedByUser = ratingProvidedByUser;
    }

    public boolean isFavorite() {
        if (isFavorite.toLowerCase().equals("yes"))
            return true;
        else return false;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPriceRangeFrom() {
        return priceRangeFrom;
    }

    public void setPriceRangeFrom(String priceRangeFrom) {
        this.priceRangeFrom = priceRangeFrom;
    }

    public String getPriceRangeTo() {
        return priceRangeTo;
    }

    public void setPriceRangeTo(String priceRangeTo) {
        this.priceRangeTo = priceRangeTo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getTodayTimings() {
        String timings;
        if(todayTimings.getStartTime().equals("null") || todayTimings.getEndTime().equals("null"))
            timings="Closed";
        else timings=todayTimings.getStartTime()+" - "+todayTimings.getEndTime();
        return timings;
    }

    public void setTodayTimings(BoTiming todayTimings) {
        this.todayTimings = todayTimings;
    }

    @SerializedName("today")
    public BoTiming todayTimings;
}
