package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;


/**
 * Created by Irfan on 09/01/16.
 */

@Parcel
public class BoEvent {

    @SerializedName("dealId")
    public String dealId;

    @SerializedName("businessName")
    public String businessName;

    @SerializedName("businessImageLogoUrl")
    public String businessLogoUrl;

    @SerializedName("dealImageurl")
    public String dealImageurl;


    @SerializedName("dealHeading")
    public String dealHeading;

    @SerializedName("currencySymbol")
    public String currencySymbol;

    @SerializedName("price")
    public String price;
    @SerializedName("priceRangeFrom")
    public String priceRangeFrom;
    @SerializedName("priceRangeTo")
    public String priceRangeTo;
    @SerializedName("latitude")
    public String latitude;
    @SerializedName("longitude")
    public String longitude;
    @SerializedName("distance")
    public String distance;
    @SerializedName("businessWebsiteUrl")

    public String businessWebsiteUrl;
    @SerializedName("type")

    public String type;

    public String getPriceRangeFrom() {
        return priceRangeFrom;
    }

    public void setPriceRangeFrom(String priceRangeFrom) {
        this.priceRangeFrom = priceRangeFrom;
    }

    public String getPriceRangeTo() {
        return priceRangeTo;
    }

    public void setPriceRangeTo(String priceRangeTo) {
        this.priceRangeTo = priceRangeTo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBusinessWebsiteUrl() {
        if (!businessWebsiteUrl.startsWith("http")) {
            businessWebsiteUrl = "http://" + businessWebsiteUrl;
        }
        return businessWebsiteUrl;
    }

    public void setBusinessWebsiteUrl(String businessWebsiteUrl) {
        this.businessWebsiteUrl = businessWebsiteUrl;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getDealHeading() {
        return dealHeading;
    }

    public void setDealHeading(String dealHeading) {
        this.dealHeading = dealHeading;
    }

    public String getDealImageurl() {
        return dealImageurl;
    }

    public void setDealImageurl(String dealImageurl) {
        this.dealImageurl = dealImageurl;
    }

    public String getBusinessLogoUrl() {
        return businessLogoUrl;
    }

    public void setBusinessLogoUrl(String businessLogoUrl) {
        this.businessLogoUrl = businessLogoUrl;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }
}
