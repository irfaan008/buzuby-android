package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Irfan on 09/01/16.
 */
@Parcel
public class PojoUser {

    @SerializedName("status")
    public int status;
    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public ArrayList<BoUser> allUser;

    public ArrayList<BoUser> getAllUser() {
        return allUser;
    }

    public void setAllUser(ArrayList<BoUser> allUser) {
        this.allUser = allUser;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;

    }
}
