package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;


/**
 * Created by Irfan on 09/01/16.
 */

@Parcel
public class BoCategory {

    @SerializedName("itemId")
    public String itemId;
    @SerializedName("itemName")
    public String itemName;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

}
