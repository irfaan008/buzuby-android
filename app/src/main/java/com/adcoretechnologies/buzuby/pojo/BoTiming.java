package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;


/**
 * Created by Irfan on 09/01/16.
 */

@Parcel
public class BoTiming {
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @SerializedName("startTime")
    public String startTime;

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @SerializedName("endTime")
    public String endTime;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @SerializedName("day")
    public String day;


}
