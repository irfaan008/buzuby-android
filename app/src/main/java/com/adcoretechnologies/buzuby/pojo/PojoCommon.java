package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Irfan on 09/01/16.
 */
@Parcel
public class PojoCommon {

    @SerializedName("status")
    public int status;
    @SerializedName("message")
    public String message;
    @SerializedName("total_records")
    public int total_records;

    public int getTotal_records() {
        return total_records;
    }

    public void setTotal_records(int total_records) {
        this.total_records = total_records;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
