package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Irfan on 09/01/16.
 */
@Parcel
public class PojoCategory {

    @SerializedName("status")
    public int status;
    @SerializedName("message")
    public String message;
    @SerializedName("total_records")
    public int total_records;
    @SerializedName("data")
    public ArrayList<BoCategory> allCategory;

    public ArrayList<BoCategory> getAllCategory() {
        return allCategory;
    }

    public void setAllCategory(ArrayList<BoCategory> allHomeBox) {
        this.allCategory = allHomeBox;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotal_records() {
        return total_records;
    }

    public void setTotal_records(int total_records) {
        this.total_records = total_records;
    }
}
