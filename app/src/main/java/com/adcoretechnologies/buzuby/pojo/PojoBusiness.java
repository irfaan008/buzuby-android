package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Irfan on 09/01/16.
 */
@Parcel
public class PojoBusiness {

    @SerializedName("status")
    public int status;
    @SerializedName("message")
    public String message;
    @SerializedName("total_records")
    public int totalRecords;
    @SerializedName("data")
    public ArrayList<BoBusiness> allBusinesss;

    public ArrayList<BoBusiness> getAllBusinesss() {
        return allBusinesss;
    }

    public void setAllBusinesss(ArrayList<BoBusiness> allBusinesss) {
        this.allBusinesss = allBusinesss;
    }

    public int getTotal_records() {
        return totalRecords;
    }

    public void setTotal_records(int total_records) {
        this.totalRecords = total_records;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
