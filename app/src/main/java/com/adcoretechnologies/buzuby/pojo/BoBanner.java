package com.adcoretechnologies.buzuby.pojo;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;


/**
 * Created by Irfan on 09/01/16.
 */

@Parcel
public class BoBanner {

    @SerializedName("businessId")
    public String businessId;

    @SerializedName("businessName")
    public String businessName;
    @SerializedName("businessImageLogoUrl")

    public String businessLogoUrl;
    @SerializedName("businessImageBannerUrl")
    public String businessBannerUrl;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessLogoUrl() {
        return businessLogoUrl;
    }

    public void setBusinessLogoUrl(String businessLogoUrl) {
        this.businessLogoUrl = businessLogoUrl;
    }

    public String getBusinessBannerUrl() {
        return businessBannerUrl;
    }

    public void setBusinessBannerUrl(String businessBannerUrl) {
        this.businessBannerUrl = businessBannerUrl;
    }

}
