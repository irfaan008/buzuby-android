package com.adcoretechnologies.buzuby;

import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.adcoretechnologies.buzuby.notification.NotificationActivity;
import com.adcoretechnologies.buzuby.notification.ShowNotification;
import com.adcoretechnologies.buzuby.pojo.BoUser;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;
import com.adcoretechnologies.buzuby.util.Pref;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvEmail)
    TextView tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NavigationView navView = (NavigationView) findViewById(R.id.nav_view);


        ButterKnife.bind(this, navView.getHeaderView(0));

        setupViews();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_favorite);
        FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.framelayout, FragmentFavorite.newInstance());
        ft.commit();
        getSupportActionBar().setTitle("My Favorites");
    }

    private void setupViews() {
        tvName.setText(Pref.Read(getApplicationContext(), Const.PREF_FULL_NAME));
        tvEmail.setText(Pref.Read(getApplicationContext(), Const.PREF_EMAIL));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Intent intent = null;

        int id = item.getItemId();

        if (id == R.id.nav_preference) {
            intent = new Intent(this, PreferenceActivity.class);
        } else if (id == R.id.nav_search) {
            intent = new Intent(this, SearchActivity.class);
        } else if (id == R.id.nav_events) {
            intent = new Intent(this, BrowseEventsActivity.class);
        } else if (id == R.id.nav_favorite) {
//            intent = new Intent(this, BrowseFavoritesActivity.class);
            FragmentManager fm = getFragmentManager();
            android.app.FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.framelayout, FragmentFavorite.newInstance());
            ft.commit();

            getSupportActionBar().setTitle("My Favorites");

        } else if (id == R.id.nav_notification) {
            notification();

        } else if (id == R.id.nav_share) {
            shareApp();

        } else if (id == R.id.nav_logout) {
            BoUser user = new BoUser();
            Common.setUserDetail(getApplicationContext(), user);

            intent = new Intent(this, LoginActivity.class);
            finish();
        }
        if (intent != null)
            startActivity(intent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void notification() {

        Intent i = new Intent (this, NotificationActivity.class);
        startActivity(i);

        // Code for Firebase Notification //
    }

    private void shareApp() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Buzuby");
            String sAux = "\nI would like to recommend you this application\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.adcoretechnologies.buzuby \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) { //e.toString();

        }
    }

//    mRegistrationBroadcastReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            // checking for type intent filter
//            if (intent.getAction().equals(Const.REGISTRATION_COMPLETE)) {
//                // gcm successfully registered
//                // now subscribe to `global` topic to receive app wide notifications
//                FirebaseMessaging.getInstance().subscribeToTopic(Const.TOPIC_GLOBAL);
//
//                displayFirebaseRegId();
//
//            } else if (intent.getAction().equals(Const.PUSH_NOTIFICATION)) {
//                // new push notification is received
//
////                String message = intent.getStringExtra("message");
////
////                Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
////
////                txtMessage.setText(message);
//            }
//        }
//    };
//
//    displayFirebaseRegId();
//}
    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Const.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);
//
//        if (!TextUtils.isEmpty(regId))
//            txtRegId.setText("Firebase Reg Id: " + regId);
//        else
//            txtRegId.setText("Firebase Reg Id is not received yet!");
    }

    @Override
    protected void onResume() {
        super.onResume(); // register GCM registration complete receiver

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Const.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Const.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        ShowNotification.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}

