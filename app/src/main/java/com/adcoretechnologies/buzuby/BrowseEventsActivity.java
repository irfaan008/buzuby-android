package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.adcoretechnologies.buzuby.adapter.AdapterEvents;
import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.pojo.BoBanner;
import com.adcoretechnologies.buzuby.pojo.BoEvent;
import com.adcoretechnologies.buzuby.pojo.PojoBanner;
import com.adcoretechnologies.buzuby.pojo.PojoEvent;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;
import com.adcoretechnologies.buzuby.util.Pref;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 07/01/16.
 */
public class BrowseEventsActivity extends BaseActivity {

    @BindView(R.id.rvEvent)
    RecyclerView mRecyclerView;

    @BindView(R.id.mViewFlipper)
    ViewFlipper mViewFlipper;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    @BindView(R.id.mCollapsingToolbar)
    CollapsingToolbarLayout mCollapsingToolbar;

    AdapterEvents adapterEvents;
    IAppService methods;
    boolean isPrivate = false;
    String businessName = "";
    private ProgressDialog dialog;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paralax_toolbar);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        setSupportActionBar(toolbar);
        ab.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        dialog = new ProgressDialog(this);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Intent intent = getIntent();
        String businessId = "";
        if (intent != null) {
            if (intent.hasExtra(Const.EXTRA_BUSINESS_ID)) {
                businessId = intent.getExtras().getString(Const.EXTRA_BUSINESS_ID);
                businessName = intent.getExtras().getString(Const.EXTRA_BUSINESS_NAME);
                isPrivate = true;
            }

        }
        if (!isPrivate) {
            getBanner();
            getEvents(Pref.Read(getApplicationContext(), Const.PREF_LATITUDE), Pref.Read(getApplicationContext(), Const.PREF_LONGITUDE), Pref.Read(getApplicationContext(), Const.PREF_RADIUS), Pref.Read(getApplicationContext(), Const.PREF_CATEGORYID), Pref.Read(getApplicationContext(), Const.PREF_SUBCATEGORYID), Pref.Read(getApplicationContext(), Const.PREF_SUBSUBCATEGORYID), Pref.Read(getApplicationContext(), Const.PREF_START_PRICE), Pref.Read(getApplicationContext(), Const.PREF_END_PRICE), Pref.Read(getApplicationContext(), Const.PREF_CURRENCY_TYPE));
        } else {
//            ab.setTitle(businessName);
            mViewFlipper.setVisibility(View.GONE);
            appbar.getLayoutParams().height = 300;
            getEvents(businessId, Pref.Read(getApplicationContext(), Const.PREF_LATITUDE), Pref.Read(getApplicationContext(), Const.PREF_LONGITUDE));
        }

        mCollapsingToolbar.setTitle(getString(R.string.ab_browse_events));

        configureBanner();
    }

    private void configureBanner() {
        mViewFlipper.setAutoStart(true);
        mViewFlipper.setFlipInterval(2000);
        mViewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        mViewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    private void getBanner() {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Getting Banner");
        dialog.show();

        methods.getAllBanner(Const.ACTION_GET_ALL_BANNER, Common.getToken(this), new Callback<PojoBanner>() {
            @Override
            public void success(PojoBanner pojoBanner, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojoBanner.getStatus();
                String message = pojoBanner.getMessage();
                if (code == Const.STATUS_SUCCESS)

                    BindViewBanner(pojoBanner.getAllBanner());
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while getting banners", error);

                if (dialog != null)
                    dialog.dismiss();

            }
        });

    }

    private void BindViewBanner(ArrayList<BoBanner> allBanner) {
        if (allBanner != null) {

            if (mViewFlipper != null) {
                for (int i = 0; i < allBanner.size(); i++) {
                    log("Banner " + i + " url: " + allBanner.get(i).getBusinessBannerUrl());
                    ImageView imageView = new ImageView(this);
                    Common.showBannerImage(getApplicationContext(), imageView, allBanner.get(i).getBusinessBannerUrl());

                    mViewFlipper.addView(imageView);
                    imageView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                    imageView.getLayoutParams().height = 400;
                }
                mViewFlipper.startFlipping();
            } else log("There is some problem initialising view flipper");

        }

    }


    private void getEvents(String latitude, String longitude, String radius, String categoryId, String subCategoryId, String subSubCategoryId, String startPrice, String endPrice, String currencyType) {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Getting Events & Deals based on your preference");
        dialog.show();

        methods.getDealEventsByPreference(Const.ACTION_GET_ALL_DEAL_EVENTS, Common.getToken(getApplicationContext()), Common.getUserId(getApplicationContext()), latitude, longitude, radius, categoryId, subCategoryId, subSubCategoryId, currencyType, startPrice, endPrice, new Callback<PojoEvent>() {
            @Override
            public void success(PojoEvent pojoEvent, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojoEvent.getStatus();
                String message = pojoEvent.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    BindViewEvents(pojoEvent.getAllEvents());
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while getting events based on preference", error);

                if (dialog != null)
                    dialog.dismiss();
            }
        });


    }

    private void getEvents(final String businessId, String latitude, String longitude) {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Getting Events & Deals of " + businessName);
        dialog.show();

        methods.getDealEventsByBusinessId(Const.ACTION_GET_ALL_BUSINESS_BY_BUSINESS_ID, Common.getToken(getApplicationContext()), Common.getUserId(getApplicationContext()), businessId, latitude, longitude, new Callback<PojoEvent>() {
            @Override
            public void success(PojoEvent pojoEvent, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojoEvent.getStatus();
                String message = pojoEvent.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    BindViewEvents(pojoEvent.getAllEvents());
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while getting events based on business id :" + businessId, error);

                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }

    private void BindViewEvents(ArrayList<BoEvent> allEvents) {

        adapterEvents = new AdapterEvents(this, allEvents);
        mRecyclerView.setAdapter(adapterEvents);

    }


    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);
    }

}
