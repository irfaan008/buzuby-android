package com.adcoretechnologies.buzuby.api;

import com.adcoretechnologies.buzuby.notification.PojoPush;
import com.adcoretechnologies.buzuby.pojo.PojoBanner;
import com.adcoretechnologies.buzuby.pojo.PojoBusiness;
import com.adcoretechnologies.buzuby.pojo.PojoBusinessDetail;
import com.adcoretechnologies.buzuby.pojo.PojoCategory;
import com.adcoretechnologies.buzuby.pojo.PojoCommon;
import com.adcoretechnologies.buzuby.pojo.PojoEvent;
import com.adcoretechnologies.buzuby.pojo.PojoUser;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by Irfan Raza on 22/6/15.
 * http://blog.robinchutaux.com/blog/a-smart-way-to-use-retrofit/
 * <p/>
 * Alternate of retrofit
 * https://github.com/kevinsawicki/http-request
 */
public interface IAppService {
    @Headers("Connection: close")
    @FormUrlEncoded
    @POST("/service.php/")
    void performLogin(@Field("action") String action, @Field("userName") String username, @Field("password") String password, @Field("deviceType") String deviceType, @Field("IMEI") String IMEI, @Field("pushId") String pushId, Callback<PojoUser> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void performRegistration(@Field("action") String action, @Field("userName") String username, @Field("password") String password, @Field("firstName") String firstName, @Field("lastName") String lastName, @Field("email") String email, @Field("mobileNumber") String mobileNumber, @Field("dob") String dob, @Field("address") String address, @Field("city") String city, @Field("suburb") String suburb, @Field("province") String province, @Field("country") String country, @Field("latitude") String latitude, @Field("longitude") String longitude,Callback<PojoUser> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void performQuickRegistration(@Field("action") String action, @Field("fullName") String username, @Field("email") String email, @Field("password") String password, @Field("deviceType") String deviceType, @Field("IMEI") String IMEI, @Field("pushId") String pushId,  Callback<PojoUser> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void getAllCategory(@Field("action") String action, @Field("itemId") String itemId, Callback<PojoCategory> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void getAllBanner(@Field("action") String action, @Field("token") String token, Callback<PojoBanner> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void getDealEventsByPreference(@Field("action") String action, @Field("token") String token, @Field("userId") String userId, @Field("latitude") String latitude, @Field("longitude") String longitude, @Field("radius") String radius, @Field("categoryId") String categoryId, @Field("subCategoryId") String subCategoryId, @Field("subSubCategoryId") String subSubCategoryId, @Field("currencySymbol") String currencySymbol, @Field("priceFrom") String priceFrom, @Field("priceTo") String priceTo, Callback<PojoEvent> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void getDealEventsByBusinessId(@Field("action") String action, @Field("token") String token, @Field("userId") String userId, @Field("businessId") String businessId, @Field("latitude") String latitude, @Field("longitude") String longitude, Callback<PojoEvent> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void getBusinessByPreference(@Field("action") String action, @Field("token") String token, @Field("userId") String userId, @Field("keyword") String keyword, @Field("categoryId") String categoryId, @Field("subCategoryId") String subCategoryId, @Field("subSubCategoryId") String subSubCategoryId, Callback<PojoBusiness> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void addBusinessToFavorite(@Field("action") String action, @Field("token") String token, @Field("userId") String userId, @Field("businessId") String businessId, Callback<PojoCommon> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void addBusinessRating(@Field("action") String action, @Field("token") String token, @Field("userId") String userId, @Field("businessId") String businessId, @Field("rating") String rating, Callback<PojoCommon> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void getBusinessDetailById(@Field("action") String action, @Field("token") String token, @Field("userId") String userId, @Field("businessId") String businessId, Callback<PojoBusinessDetail> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void updatePushId(@Field("action") String action, @Field("IMEI") String imei, @Field("userId") String userId, @Field("pushId") String pushId, Callback<PojoPush> callback);

    @FormUrlEncoded
    @POST("/service.php/")
    void getMyFavoriteList(@Field("action") String action, @Field("token") String token, @Field("userId") String userId, Callback<PojoBusiness> callback);


    @FormUrlEncoded
    @POST("/service.php/")
    void getAllPushNotification(@Field("action") String action, @Field("userId") String userId,Callback<PojoPush> callback);

}

