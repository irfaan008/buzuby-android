package com.adcoretechnologies.buzuby.api;


import android.content.Context;
import android.util.Log;

import com.adcoretechnologies.buzuby.BaseActivity;
import com.adcoretechnologies.buzuby.util.Const;

import retrofit.RestAdapter;

/**
 * Created by Irfan on 22/09/15.
 */
public class APIHelper {

    BaseActivity activity;
    Context context;
    String url = Const.API_BASE_URL;

    IAppService methods;


    public APIHelper(BaseActivity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(url)
                .build();
        methods = restAdapter.create(IAppService.class);

    }

    public static IAppService getAppServiceMethod() {
        String url = Const.API_BASE_URL;

        RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(url)
                .build();
        return restAdapter.create(IAppService.class);
    }


    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);

    }
}
