package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.customviews.FontableTextView;
import com.adcoretechnologies.buzuby.pojo.BoUser;
import com.adcoretechnologies.buzuby.pojo.PojoUser;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;
import com.adcoretechnologies.buzuby.util.Pref;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 07/01/16.
 */
public class RegisterActivity extends BaseActivity {


    IAppService methods;
    @BindView(R.id.etFirstName)
    EditText etFirstName;
    @BindView(R.id.etSurName)
    EditText etSurName;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;
    @BindView(R.id.tvBack)
    FontableTextView tvBack;
    @BindView(R.id.tvRegister)
    FontableTextView tvRegister;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);

        dialog = new ProgressDialog(this);
        if (Const.DEBUG_TEST)
            configureTest();
    }

    private void configureTest() {
        String dummyPrefix = "test192";
        etPassword.setText(dummyPrefix);
        etConfirmPassword.setText(dummyPrefix);
        etFirstName.setText(dummyPrefix);
        etSurName.setText(dummyPrefix);
        etEmail.setText(dummyPrefix + "@gmail.com");
    }

    @OnClick(R.id.tvBack)
    public void BackClick() {
        finish();
    }

    @OnClick(R.id.tvRegister)
    public void RegisterClick() {
        String password = etPassword.getText().toString().trim();
        String lastName = etSurName.getText().toString().trim();
        String firstName = etFirstName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();
        String fullName = firstName + " " + lastName;
        if (validateInput(password, fullName, email, confirmPassword))
            performRegistration(password, fullName, email);
        else
            Toast.makeText(getApplicationContext(), "Please provide all input correctly", Toast.LENGTH_LONG).show();
    }

    private boolean validateInput(String password, String fullName, String email, String confirmPassword) {

        if (password.isEmpty() || fullName.isEmpty() || email.isEmpty())
            return false;
        else if (!password.equals(confirmPassword)) {
            etConfirmPassword.setBackgroundColor(Color.RED);
            return false;
        } else
            return true;
    }

    private void performRegistration(String password, String fullName, String email) {
        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Registering user");
        dialog.show();

        String imei = Common.getImei(getApplicationContext());
        String token = FirebaseInstanceId.getInstance().getToken();

        methods.performQuickRegistration(Const.ACTION_REGISTER_QUICK, fullName, email, password, "android",imei,token, new Callback<PojoUser>() {
            @Override
            public void success(PojoUser pojoUser, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojoUser.getStatus();
                String message = pojoUser.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    BindViewItems(pojoUser.getAllUser().get(0));
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while logging in", error);

                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }

    private void BindViewItems(BoUser user) {

        if (user != null) {
            Pref.Write(getApplicationContext(), Const.PREF_USER_ID, user.getUserId());
            Pref.Write(getApplicationContext(), Const.PREF_FULL_NAME, user.getFirstName());
            Pref.Write(getApplicationContext(), Const.PREF_AUTH_TOKEN, user.getAuthToken());
            Pref.Write(getApplicationContext(), Const.PREF_EMAIL, user.getEmail());

            Toast.makeText(getApplicationContext(), "You are registered successfully", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else
            Toast.makeText(getApplicationContext(), "Can't register at this time. Please connect with system admin", Toast.LENGTH_LONG).show();

    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);
    }
}
