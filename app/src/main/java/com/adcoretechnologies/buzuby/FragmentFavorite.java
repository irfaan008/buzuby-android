package com.adcoretechnologies.buzuby;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.adapter.AdapterFavorite;
import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.customviews.FontableTextView;
import com.adcoretechnologies.buzuby.pojo.BOEventData;
import com.adcoretechnologies.buzuby.pojo.BoBusiness;
import com.adcoretechnologies.buzuby.pojo.PojoBusiness;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 18/04/16.
 */
public class FragmentFavorite extends Fragment {

    BaseActivity activity;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.ftvNoData)
    FontableTextView ftvNoData;
    AdapterFavorite adapterBusiness;
    IAppService methods;
    private ProgressDialog dialog;
    private RecyclerView.LayoutManager mLayoutManager;
    Context ctx;

    public static FragmentFavorite newInstance(){
        return new FragmentFavorite();
    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (BaseActivity) getActivity();
        ctx=activity.getApplicationContext();
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_favorites,
                    container, false);
            ButterKnife.bind(this,rootView);

            dialog = new ProgressDialog(activity);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(activity);
            mRecyclerView.setLayoutManager(mLayoutManager);

            try {
                getAllItems();
            } catch (Exception ex) {
                log("Exception while fetching data from api");
            }

        } catch (Exception ex) {
            log("Exception while loading buzzing layout");
        }

        return rootView;
    }

    @Override
    public void onResume() {

        super.onResume();

    }

    private void getAllItems() {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Getting Business from your favorite list");
        dialog.show();

        methods.getMyFavoriteList(Const.ACTION_GET_MY_FAVORITE_LIST, Common.getToken(ctx), Common.getUserId(ctx), new Callback<PojoBusiness>() {
            @Override
            public void success(PojoBusiness pojo, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojo.getStatus();
                String message = pojo.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    BindViewBusiness(pojo.getAllBusinesss());
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while getting Business based on favorite list", error);

                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }


    private void BindViewBusiness(ArrayList<BoBusiness> allBusiness) {

        updateViews(allBusiness.size());
        adapterBusiness = new AdapterFavorite(activity, allBusiness);
        mRecyclerView.setAdapter(adapterBusiness);
    }


    private void updateViews(int size) {
        if(size>0){
            ftvNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
        else
        {
            ftvNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    public void onEventMainThread(BOEventData eventData) {
        int eventType = eventData.eventType;
        if(eventType==Const.EVENT_FAVORITE){
            int size=Integer.parseInt(eventData.data);
            updateViews(size);
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);

    }

}
