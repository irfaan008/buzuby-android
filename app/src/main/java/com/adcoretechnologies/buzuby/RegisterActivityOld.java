package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.pojo.BoUser;
import com.adcoretechnologies.buzuby.pojo.PojoUser;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;
import com.adcoretechnologies.buzuby.util.Pref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 07/01/16.
 */
public class RegisterActivityOld extends BaseActivity {

    @BindView(R.id.tvBack)
    TextView tvBack;
    @BindView(R.id.tvRegister)
    TextView tvRegister;
    @BindView(R.id.editAddress)
    EditText etAddress;
    @BindView(R.id.editCity)
    EditText etCity;
    @BindView(R.id.editConfirmPassword)
    EditText etConfirmPassword;
    @BindView(R.id.editCountry)
    EditText etCountry;
    @BindView(R.id.editCountryCode)
    EditText etCountryCode;
    @BindView(R.id.editDay)
    EditText etDay;
    @BindView(R.id.editMonth)
    EditText etMonth;
    @BindView(R.id.editYear)
    EditText etYear;
    @BindView(R.id.editMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.editPassword)
    EditText etPassword;
    @BindView(R.id.editEmail)
    EditText etEmail;
    @BindView(R.id.editSuburb)
    EditText etSuburb;
    @BindView(R.id.editProvince)
    EditText etProvince;
    @BindView(R.id.editUserName)
    EditText etUserName;
    @BindView(R.id.editSurName)
    EditText etLastName;
    @BindView(R.id.editFirstName)
    EditText etFirstName;
    IAppService methods;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_old);

        ButterKnife.bind(this);

        dialog = new ProgressDialog(this);
        if (Const.DEBUG_TEST)
            configureTest();
    }

    private void configureTest() {
        String dummyPrefix = "test1";
        etUserName.setText(dummyPrefix);
        etPassword.setText(dummyPrefix);
        etConfirmPassword.setText(dummyPrefix);
        etFirstName.setText(dummyPrefix);
        etLastName.setText(dummyPrefix);
        etAddress.setText(dummyPrefix);
        etCity.setText(dummyPrefix);
        etSuburb.setText(dummyPrefix);
        etProvince.setText(dummyPrefix);
        etCountry.setText(dummyPrefix);
        etCountryCode.setText("91");
        etMobileNumber.setText("1234567890");
        etEmail.setText(dummyPrefix + "@gmail.com");
        etDay.setText("1");
        etMonth.setText("12");
        etYear.setText("1990");
    }

    @OnClick(R.id.tvBack)
    public void BackClick() {
        finish();
    }

    @OnClick(R.id.tvRegister)
    public void RegisterClick() {
        String userName = etUserName.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String firstName = etFirstName.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String city = etCity.getText().toString().trim();
        String suburb = etSuburb.getText().toString().trim();
        String province = etProvince.getText().toString().trim();
        String country = etCountry.getText().toString().trim();
        String mobileNumber = etMobileNumber.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String day = etDay.getText().toString().trim();
        String month = etMonth.getText().toString().trim();
        String year = etYear.getText().toString().trim();
        String countryCode = etCountryCode.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();

        double latitude = 0, longitude = 0;
        if (validateInput(userName, password, firstName, lastName, address, city, suburb, province, country, mobileNumber, email, day, month, year, countryCode, confirmPassword))
            performRegistration(userName, password, firstName, lastName, address, city, suburb, province, country, countryCode + " " + mobileNumber, email, day + "/" + month + "/" + year, latitude, longitude);
        else
            Toast.makeText(getApplicationContext(), "Please provide all input correctly", Toast.LENGTH_LONG).show();
    }

    private boolean validateInput(String userName, String password, String firstName, String lastName, String address, String city, String suburb, String province, String country, String countryCode, String mobileNumber, String email, String day, String month, String year, String confirmPassword) {

        if (userName.isEmpty() || password.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || address.isEmpty() || city.isEmpty() || suburb.isEmpty() || province.isEmpty() || country.isEmpty() || mobileNumber.isEmpty() || email.isEmpty() || day.isEmpty() || month.isEmpty() || year.isEmpty() || userName.isEmpty() || confirmPassword.isEmpty() || countryCode.isEmpty())
            return false;
        else if (!password.equals(confirmPassword)) {
            etConfirmPassword.setBackgroundColor(Color.RED);
            return false;
        } else
            return true;
    }

    private void performRegistration(String userName, String password, String firstName, String lastName, String address, String city, String suburb, String province, String country, String mobileNumber, String email, String dob, double latitude, double longitude) {
        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Registering user");
        dialog.show();

        methods.performRegistration(Const.ACTION_DO_REGISTRATION, userName, password, firstName, lastName, email, mobileNumber, dob, address, city, suburb, province, country, latitude + "", longitude + "", new Callback<PojoUser>() {
            @Override
            public void success(PojoUser pojoUser, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojoUser.getStatus();
                String message = pojoUser.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    BindViewItems(pojoUser.getAllUser().get(0));
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while logging in", error);

                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }

    private void BindViewItems(BoUser user) {

        if (user != null) {
            Pref.Write(getApplicationContext(), Const.PREF_USER_ID, user.getUserId());
            Pref.Write(getApplicationContext(), Const.PREF_FULL_NAME, user.getFirstName() + " " + user.getLastName());
            Pref.Write(getApplicationContext(), Const.PREF_AUTH_TOKEN, user.getAuthToken());
            Pref.Write(getApplicationContext(), Const.PREF_ADDRESS, user.getAddress() + ", " + user.getCity() + ", " + user.getSuburb() + ", " + user.getProvince() + ", " + user.getCountry());
            Pref.Write(getApplicationContext(), Const.PREF_DOB, user.getDOB());
//            Pref.Write(getApplicationContext(), Const.PREF_LATITUDE, user.getLatitude());
            Pref.Write(getApplicationContext(), Const.PREF_EMAIL, user.getEmail());
//            Pref.Write(getApplicationContext(), Const.PREF_LONGITUDE, user.getLongitude());
            Pref.Write(getApplicationContext(), Const.PREF_MOBILENUMBER, user.getMobileNumber());

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else
            Toast.makeText(getApplicationContext(), "Can't register at this time. Please connect with system admin", Toast.LENGTH_LONG).show();

    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);
    }
}
