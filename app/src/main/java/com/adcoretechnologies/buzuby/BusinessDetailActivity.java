package com.adcoretechnologies.buzuby;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.adcoretechnologies.buzuby.api.APIHelper;
import com.adcoretechnologies.buzuby.api.IAppService;
import com.adcoretechnologies.buzuby.customviews.FontableTextView;
import com.adcoretechnologies.buzuby.pojo.BOEventData;
import com.adcoretechnologies.buzuby.pojo.BoBusinessDetail;
import com.adcoretechnologies.buzuby.pojo.PojoBusinessDetail;
import com.adcoretechnologies.buzuby.pojo.PojoCommon;
import com.adcoretechnologies.buzuby.util.Common;
import com.adcoretechnologies.buzuby.util.Const;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Irfan on 07/01/16.
 */
public class BusinessDetailActivity extends BaseActivity {

    String businessId;
    BoBusinessDetail item;
    Context context;

    @BindView(R.id.ivBusinessLogo)
    ImageView ivLogo;
    @BindView(R.id.ftvBusinessName)
    FontableTextView ftvBusinessName;
    @BindView(R.id.ivBusinessImage)
    ImageView ivBusinessImage;
    @BindView(R.id.ftvSpeciality)
    FontableTextView ftvSpeciality;
    @BindView(R.id.ftvDescription)
    FontableTextView ftvDescription;
    @BindView(R.id.ftvAmenities)
    FontableTextView ftvAmenities;
    @BindView(R.id.ftvPrice)
    FontableTextView ftvPrice;
    @BindView(R.id.ftvAddFavorite)
    FontableTextView ftvAddFavorite;
    @BindView(R.id.ftvLocate)
    FontableTextView ftvLocate;
    @BindView(R.id.ftvContact)
    FontableTextView ftvContact;
    @BindView(R.id.ftvWebsite)
    FontableTextView ftvWebsite;
    @BindView(R.id.wvTimings)
    WebView wvTimings;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;


    IAppService methods;
    boolean isTest = Const.DEBUG_TEST;
    DialogRating rating;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_detail);

        ButterKnife.bind(this);
        context = getApplicationContext();

        dialog = new ProgressDialog(this);
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
//
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(Const.EXTRA_BUSINESS_NAME)) {
                String businessName = intent.getExtras().getString(Const.EXTRA_BUSINESS_NAME);
                ab.setTitle(businessName);

            }
            if (intent.hasExtra(Const.EXTRA_BUSINESS_ID)) {
                businessId = intent.getExtras().getString(Const.EXTRA_BUSINESS_ID);
                getBusinessDetailById(businessId);
            } else
                endActivity();
        } else endActivity();
        getBusinessDetailById(businessId);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void getBusinessDetailById(String businessId) {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Fetching Business detail");
        dialog.show();

        methods.getBusinessDetailById(Const.ACTION_GET_BUSINESS_DETAIL_BY_ID, Common.getToken(getApplicationContext()), Common.getUserId(getApplicationContext()), businessId, new Callback<PojoBusinessDetail>() {
            @Override
            public void success(PojoBusinessDetail pojo, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojo.getStatus();
                int record = pojo.getTotal_records();
                String message = pojo.getMessage();
                if (code == Const.STATUS_SUCCESS) {
                    //  Todo replace with total records
                    if (pojo.getAllBusinesss().size() > 0)
                        BindBusinessDetail(pojo.getAllBusinesss().get(0));
                    else
                        Toast.makeText(getApplicationContext(), "Business detail not available", Toast.LENGTH_LONG).show();
                } else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while getting Business based on preference", error);

                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }

    private void BindBusinessDetail(BoBusinessDetail boBusiness) {
        item = boBusiness;

        Common.showBusinessLogo(getApplicationContext(), ivLogo, item.getBusinessLogoUrl());
        ftvBusinessName.setText(item.getBusinessName());
        Common.showBannerImage(getApplicationContext(), ivBusinessImage, item.getBusinessImageUrl());
        ratingBar.setRating(Float.parseFloat(item.getRating()));
        ftvSpeciality.setText(item.getSpecializeIn());
        ftvDescription.setText(item.getDescription());
        ftvAmenities.setText(Html.fromHtml(item.getAmenities()));
//        ftvTiming.setText(Html.fromHtml(item.getStoreTimings()));

        wvTimings.loadData(item.getStoreTimings(),"text/html", "UTF-8");

        ftvPrice.setText(item.getPriceRange());
        ftvLocate.setText(item.getFullAddress());
        ftvContact.setText(item.getPhone());
        ftvWebsite.setText(item.getBusinessWebsiteUrl());

        updateFavoriteView(item.isFavorite(), ftvAddFavorite);

        /*
        The reason for setOnClickListener() not working is that RatingBar overrides onTouchEvent() (actually its super class, AbsSeekBar, does) and never let View take care of it, so View#performClick() is never called
         */
        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    RatingClick(businessId, item.getBusinessName(), item.getRatingProvidedByUser());
                }
                return true;
            }
        });

    }

    private void endActivity() {
        if (isTest)
            businessId = "102";
        else {
            Toast.makeText(getApplicationContext(), "Something went wrong. Business detail not available.", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @OnClick(R.id.ftvLocate)
    public void LocateClick() {
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f", Double.parseDouble(item.getLatitude()), Double.parseDouble(item.getLongitude()));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }

    @OnClick(R.id.ftvWebsite)
    public void WebsiteClick() {
        Intent intent = new Intent(this, InbuiltBrowserActivity.class);
        intent.putExtra(Const.EXTRA_BUSINESS_NAME, item.getBusinessName());
        intent.putExtra(Const.EXTRA_BUSINESS_URL, item.getBusinessWebsiteUrl());
        startActivity(intent);
    }

    @OnClick(R.id.ftvContact)
    public void ContactClick() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + item.getPhone()));
        startActivity(intent);
    }

    public void RatingClick(String businessId, String businessName, String myRating) {
        if (rating == null) {
            rating = DialogRating.newInstance(businessId, myRating, businessName);
        }
        rating.show(getSupportFragmentManager(), "dialogRating");
    }


    @OnClick(R.id.ftvAddFavorite)
    public void AddToFavoriteClick() {
        addBusinessToFavoriteList(businessId, ftvAddFavorite);
    }

    private void addBusinessToFavoriteList(String businessId) {

        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Updating favorite list");
        dialog.show();

        methods.addBusinessToFavorite(Const.ACTION_ADD_BUSINESS_TO_FAVORITE, Common.getToken(this), Common.getUserId(this), businessId, new Callback<PojoCommon>() {
            @Override
            public void success(PojoCommon pojo, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojo.getStatus();
                String message = pojo.getMessage();
                if (code == Const.STATUS_SUCCESS)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                else if (code == Const.STATUS_ERROR)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while performing favorite ", error);

                if (dialog != null)
                    dialog.dismiss();

            }
        });


    }


    private void addBusinessToFavoriteList(String businessId, final FontableTextView ftvAddFavorite) {
        if (methods == null)
            methods = APIHelper.getAppServiceMethod();

        dialog.setTitle("Please wait");
        dialog.setMessage("Updating favorite list");
        dialog.show();

        methods.addBusinessToFavorite(Const.ACTION_ADD_BUSINESS_TO_FAVORITE, Common.getToken(context), Common.getUserId(context), businessId, new Callback<PojoCommon>() {
            @Override
            public void success(PojoCommon pojo, Response response) {
                if (dialog != null)
                    dialog.dismiss();
                double code = pojo.getStatus();
                String message = pojo.getMessage();
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                if (code == Const.STATUS_SUCCESS) {
                    updateFavoriteView(message.contains("added"), ftvAddFavorite);


                } else if (code == Const.STATUS_ERROR)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();

            }

            @Override
            public void failure(RetrofitError error) {
                Common.printException("Error while performing favorite ", error);

                if (dialog != null)
                    dialog.dismiss();

            }
        });


    }

    private void updateFavoriteView(boolean isFavorite, FontableTextView ftvAddFavorite) {
        if (isFavorite)
            ftvAddFavorite.setText("Remove from favorite");
        else
            ftvAddFavorite.setText("Add to favorite");
    }

    public void onEventMainThread(BOEventData eventData) {
        int eventType = eventData.eventType;
        if (eventType == Const.EVENT_RATING) {
            String data = eventData.data;
            ratingBar.setRating(Float.parseFloat(data));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);
    }
}
